### Hi 👋

My name is Anri -Mylloon-, welcome to my Forgejo instance!

Feel free to check [my website for more info about me](https://www.mylloon.fr) 😊

<br>

[All my project are hosted here](https://git.mylloon.fr/Anri?tab=repositories),
feel free to check them and contribute, registration is open and some 3rd parties
method are available.

There is all my [licence repositories there](https://git.mylloon.fr/Paris8)
and the [ones from my master there](https://git.mylloon.fr/Paris7).
With help, I develop some bots [available there](https://git.mylloon.fr/ConfrerieDuKassoulait).

<br>

I know that all the code there is bad, but I'm happy to say that this
forge is being archived by [Software Heritage](https://www.softwareheritage.org/).
